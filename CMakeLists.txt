cmake_minimum_required(VERSION 3.0)
project(t3)

set(CMAKE_CXX_FLAGS "-ggdb -std=c++17 -DNO__SLEEP_NA_PALE__")


set(SRC_DIR "src")

include_directories("inc")

add_executable(t3
    ${SRC_DIR}/main.cc
    ${SRC_DIR}/sem.cc
    ${SRC_DIR}/entities.cc
    )

target_link_libraries(t3 pthread)
