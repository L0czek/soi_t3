#include <iostream>
#include <iterator>
#include <chrono>
#include "state.hpp"
#include "entities.hpp"

using namespace std::chrono_literals;

template<typename Entity>
void spawn(State& state,std::size_t n, std::back_insert_iterator<std::vector<std::thread>> it) {
    for(std::size_t i=0; i < n; ++i)
        it = Entity::spawn(state);
}

int main() {
    State state;
        
    auto threads = std::vector<std::thread>();
    spawn<EvenProducer>(state, 1, std::back_inserter(threads));
    spawn<OddProducer>(state, 1, std::back_inserter(threads));
    spawn<EvenConsumer>(state, 1, std::back_inserter(threads));
    spawn<OddConsumer>(state, 1, std::back_inserter(threads));
    
    state.inUse.V(); 
    for(;;) {
        state.inUse.P();
        for(const auto &i : state.queue) {
            std::cout << i << "    ";
        }
        std::cout << "OP " <<  state.oddProducersWaiting  << " EP " <<   state.evenProducersWaiting << " OC " <<   state.oddConsumersWaiting << " EC " <<   state.evenConsumersWaiting << "\n"; 

        state.inUse.V();

        std::this_thread::sleep_for(1s);
        puts("\n\n");
        
    }
}
