#include "entities.hpp"
#include <iostream>
#include <chrono>

using namespace std::chrono_literals;

void EvenProducer::thread_loop() {
    for(;;) {
        
        state.inUse.P();

        if(!EvenProducer::canProduce(state)) {
            state.evenProducersWaiting++;
            state.inUse.V();
            state.forEvenProducers.P();
            state.evenProducersWaiting--;
        }

        produce();
        
        if(OddProducer::canProduce(state) && state.oddProducersWaiting > 0) {
            state.forOddProducers.V();
        } else if(EvenConsumer::canConsume(state) && state.evenConsumersWaiting > 0) {
            state.forEvenConsumers.V();
        } else if(OddConsumer::canConsume(state) && state.oddConsumersWaiting > 0) {
            state.forOddConsumers.V();   
        } else {
            state.inUse.V();
        }   
       
        //std::this_thread::sleep_for(5ms);
    }
}
bool EvenProducer::canProduce(State& state) {
    std::size_t even_num = 0;
    for(const auto &i : state.queue) {
        if(i % 2 == 0) {
            even_num++;        
        }
    }
    return even_num < 10;
}

void EvenProducer::produce() {
#ifdef __SLEEP_NA_PALE__
    std::this_thread::sleep_for(1s);
#endif
    auto n = 2*number++;
    number %= 100;
    state.queue.push_front(n);    
}

std::thread EvenProducer::spawn(State& state) {
    return std::thread([&state](){
                 EvenProducer(state).thread_loop();
            });
}


void OddProducer::thread_loop() {
    for(;;) {
        
        state.inUse.P();

        if(!OddProducer::canProduce(state)) {
            state.oddProducersWaiting++;
            state.inUse.V();
            state.forOddProducers.P();
            state.oddProducersWaiting--;
        }

        produce();
        if(EvenConsumer::canConsume(state) && state.evenConsumersWaiting > 0) {
            state.forEvenConsumers.V();
        } else if(OddConsumer::canConsume(state) && state.oddConsumersWaiting > 0) {
            state.forOddConsumers.V();   
        } else if(EvenProducer::canProduce(state) && state.evenProducersWaiting > 0) {
            state.forEvenProducers.V();
        } else {
            state.inUse.V();
        } 
       
        //std::this_thread::sleep_for(5ms);
    }
}
bool OddProducer::canProduce(State& state) {
    std::size_t even_num = 0;
    std::size_t odd_num = 0;
    for(const auto &i : state.queue) {
        if(i % 2 == 0) {
            even_num++;        
        } else {
            odd_num++;
        }
    }
    return odd_num < even_num;
}

void OddProducer::produce() {
#ifdef __SLEEP_NA_PALE__
    std::this_thread::sleep_for(1s);
#endif
    auto n = (2*number++ + 1);
    number %= 100;
    state.queue.push_front(n);    
}

std::thread OddProducer::spawn(State& state) {
    return std::thread([&state](){
                 OddProducer(state).thread_loop();
            });
}


void OddConsumer::thread_loop() {
    for(;;) {
        state.inUse.P();
        
        if(!OddConsumer::canConsume(state)) {
            state.oddConsumersWaiting++;
            state.inUse.V();
            state.forOddConsumers.P();
            state.oddConsumersWaiting--;
        }
        
        consume();
        if(EvenProducer::canProduce(state) && state.evenProducersWaiting > 0) {
            state.forEvenProducers.V();
        } else if(OddProducer::canProduce(state) && state.oddProducersWaiting > 0) {
            state.forOddProducers.V();
        } else if(EvenConsumer::canConsume(state) && state.evenConsumersWaiting > 0) {
            state.forEvenConsumers.V();   
        } else {
            state.inUse.V();
        } 


        //std::this_thread::sleep_for(5ms);
   }    
}
void OddConsumer::consume() {
   state.queue.pop_back();
}
bool OddConsumer::canConsume(State& state) {
    return state.queue.size() >= 7 && (state.queue.back() % 2) == 1;
}
std::thread OddConsumer::spawn(State& state) {
    return std::thread([&state](){
                OddConsumer(state).thread_loop(); 
            });
}


void EvenConsumer::thread_loop() {
    for(;;) {
        state.inUse.P();
        
        if(!EvenConsumer::canConsume(state)) {
            state.evenConsumersWaiting++;
            state.inUse.V();
            state.forEvenConsumers.P();
            state.evenConsumersWaiting--;
        }

        consume();
        
        if(OddConsumer::canConsume(state) && state.oddConsumersWaiting > 0) {
            state.forOddConsumers.V();   
        } else if(OddProducer::canProduce(state) && state.oddProducersWaiting > 0) {
            state.forOddProducers.V();
        } else if(EvenProducer::canProduce(state) && state.evenProducersWaiting > 0) {
            state.forEvenProducers.V();
        } else {        
            state.inUse.V();
        }
//        std::this_thread::sleep_for(5ms);
    }    
}
void EvenConsumer::consume() {
    state.queue.pop_back();
}
bool EvenConsumer::canConsume(State& state) {
    return state.queue.size() >= 3 && (state.queue.back() % 2) == 0;
}
std::thread EvenConsumer::spawn(State& state) {
    return std::thread([&state](){
                EvenConsumer(state).thread_loop(); 
            });
}


