#pragma once

class Semaphore {
    int id;
    static int next_proj_id();
public:
    Semaphore(int value = 1);
    Semaphore(const char* path, int proj_id,int value = 1);
    Semaphore(const Semaphore& other);
    void P();
    void V();
};
