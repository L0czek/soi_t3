#pragma once

#include "state.hpp"

#include <thread>

class OddProducer {
    State& state;
    std::size_t number =0 ;
    void thread_loop();
public:
    OddProducer(State& state): state(state) {}
    void produce();
    static bool canProduce(State& state);
    static std::thread spawn(State& state);
};

class EvenProducer {
    State& state;
     std::size_t number = 0;
   void thread_loop();
public:
    EvenProducer(State& state): state(state) {}
    void produce();
    static bool canProduce(State& state);
   static std::thread spawn(State& state);
};

class OddConsumer {
    State& state;
    void thread_loop();
public:
    OddConsumer(State& state): state(state) {}
    static bool canConsume(State& state);
   void consume();
    static std::thread spawn(State& state);
};

class EvenConsumer {
    State& state;
    void thread_loop();
public:
    EvenConsumer(State& state): state(state) {}
    static bool canConsume(State& state);
   void consume();
    static std::thread spawn(State& state);
};
