#pragma once

#include <queue>
#include "sem.hpp"
#include <mutex>
struct State {
    Semaphore forOddProducers{0};
    Semaphore forEvenProducers{0};
    Semaphore forOddConsumers{0};
    Semaphore forEvenConsumers{0};
    struct mm {
        std::mutex m;
        mm() {
            m.lock();
        }
        void P() {m.lock();}
        void V() {m.unlock();}
    } ;//inUse;//, forOddProducers, forEvenProducers, forOddConsumers, forEvenConsumers;
    Semaphore inUse{0};
    std::size_t oddProducersWaiting = 0;
    std::size_t evenProducersWaiting = 0;
    std::size_t oddConsumersWaiting = 0;
    std::size_t evenConsumersWaiting = 0;

    std::deque<int> queue;
};
